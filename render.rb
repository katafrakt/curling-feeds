# encoding: utf-8

require 'feedjira'
require 'erubis'

URLS = [
	'http://pfkc.pl/rss.xml',
	'http://curling.pl/pl.rss',
	'http://pzc-rss.curling-poland.com/pzc.rss'
]

feeds = Feedjira::Feed.fetch_and_parse URLS

entries = []

feeds.each do |key, val|
	entries += val.entries if val.is_a?(Feedjira::FeedUtilities)
end

entries = entries.sort_by(&:published).reverse.take(16)

tpl = File.read('template.erb')
eruby = Erubis::Eruby.new(tpl)

context = {
	entries: entries
}

File.open('public/index.html', 'w') do |f|
	f.write(eruby.evaluate(context))
end
